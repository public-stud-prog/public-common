import { ISearchParameterDTO } from "../common/isearch-parameter.interface";
import { SortDirection } from "../../../enums/common/sort-direction.enum";
import { UpworkFeedSearchBy } from "../../../enums/upwork-feed/upwork-feed-search-by.enum";
import { UpworkFeedSortBy } from "../../../enums/upwork-feed/upwork-feed-sort-by.enum";

export interface IGetAllUpworkFeedRequest {
  searchParameters?: ISearchParameterDTO<UpworkFeedSearchBy>[];
  sortDirection?: SortDirection;
  sortBy?: UpworkFeedSortBy;
}
