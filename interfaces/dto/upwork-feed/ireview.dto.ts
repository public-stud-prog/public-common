import { ReviewType } from "../../../enums/upwork-feed/review-type.enum";

export interface IReviewDTO {
  type: ReviewType;
  comment: string;
}
