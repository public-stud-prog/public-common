import { IPaginatedResultDTO } from "../common/ipaginated-result.interface";
import { IUpworkFeedItemDTO } from "./iupwork-feed-item.dto";
import { IOptionInterface } from "../common/ioption.interface";

export interface IUpworkResponseListFeedsDto {
  items: IPaginatedResultDTO<IUpworkFeedItemDTO>;
  keywordsOptions: IOptionInterface[];
  scoreOptions: IOptionInterface[];
}
