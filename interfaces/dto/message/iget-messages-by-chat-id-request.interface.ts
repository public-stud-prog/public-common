import { ISearchParameterDTO } from "../common/isearch-parameter.interface";
import { MessageSearchBy } from "../../../enums/message/message-search-by.enum";
import { SortDirection } from "../../../enums/common/sort-direction.enum";
import { MessageSortBy } from "../../../enums/message/message-sort-by.enum";

export interface IGetMessagesByChatIdRequest {
  searchParameters?: ISearchParameterDTO<MessageSearchBy>[];
  sortDirection?: SortDirection;
  sortBy?: MessageSortBy;
}
