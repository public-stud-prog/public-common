import { IErrorDTO } from "./ierror.interface";

export interface IApiResponseDTO {
  success: boolean;
  statusCode: number;
  data?: unknown;
  error?: IErrorDTO;
}

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface IApiResponseExceptionDTO extends IApiResponseDTO {}

export interface IApiResponseGenericDTO<T> extends IApiResponseDTO {
  data: T;
}
