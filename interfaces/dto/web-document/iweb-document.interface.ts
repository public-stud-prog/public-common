import { WebDocumentSourceSection } from "../../../enums/web-document/web-document-source-section.enum";
import { IWebDocumentMetadataDTO } from "./iweb-document-metadata.interface";

export interface IWebDocumentDTO {
  id: number;
  content: string;
  metadata?: IWebDocumentMetadataDTO;
  section: WebDocumentSourceSection;
  idInSection: number;
  disabled: boolean;
  created: string;
}
