import { SortDirection } from "../../../enums/common/sort-direction.enum";
import { WebDocumentSearchBy } from "../../../enums/web-document/web-document-search-by.enum";
import { WebDocumentSortBy } from "../../../enums/web-document/web-document-sort-by.enum";
import { ISearchParameterDTO } from "../common/isearch-parameter.interface";
import { IPaginatedRequestDTO } from "../common/ipaginated-request.interface";

export interface IGetWebDocumentsRequestDTO extends IPaginatedRequestDTO {
  searchParameters?: ISearchParameterDTO<WebDocumentSearchBy>[];
  sortDirection?: SortDirection;
  sortBy?: WebDocumentSortBy;
}
