import { WebDocumentSourceSection } from "../../../enums/web-document/web-document-source-section.enum";
import { IWebDocumentMetadataDTO } from "./iweb-document-metadata.interface";
import { IInfoBlockInterfaceDto } from "./iinfo-block.interface.dto";

export interface ICreateWebDocumentDTO {
  content: string;
  section: WebDocumentSourceSection;
  idInSection: number;
  disabled: boolean;
  metadata: IWebDocumentMetadataDTO;
  infoBlock: IInfoBlockInterfaceDto[] | null;
}
