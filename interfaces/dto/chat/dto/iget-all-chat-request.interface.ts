import { ISearchParameterDTO } from "../../common/isearch-parameter.interface";
import { ChatSearchBy } from "../../../../enums/chat/chat-search-by.enum";
import { SortDirection } from "../../../../enums/common/sort-direction.enum";
import { ChatSortBy } from "../../../../enums/chat/chat-sort-by.enum";

export interface IGetAllChatsRequest {
  searchParameters?: ISearchParameterDTO<ChatSearchBy>[];
  sortDirection?: SortDirection;
  sortBy?: ChatSortBy;
}
